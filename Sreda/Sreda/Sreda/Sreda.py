import sys
import pickle


###Importi###
import sys
from sklearn.cross_validation import KFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import RandomizedPCA
from sklearn.decomposition import PCA
import operator
import numpy as np
from scipy.optimize import fmin_l_bfgs_b
import scipy.sparse as sp
from sklearn import tree
from sklearn import preprocessing
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import BernoulliNB
from sklearn.preprocessing import OneHotEncoder
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectKBest
import itertools
from sklearn.metrics import auc_score
from sklearn.cross_validation import ShuffleSplit

def append_ones(X):
    '''
    Matriki X na prvo mesto doda stolpec enk
    Potrebno za linearno regresijo
    (t0*1+t1*a1+...)
    '''
    if sp.issparse(X):
        return sp.hstack((np.ones((X.shape[0], 1)), X)).tocsr()
    else:
        return np.hstack((np.ones((X.shape[0], 1)), X))

def submit(p):
    '''
    Dobljeno predvidevanje modela zapise v csv datoteko,
    primerno za oddajo na kaggle
    '''
    f = open('submission.csv', 'w')
    f.write('Id,Action\n')
    for i in range(p.size):
        f.write('{},{:.18e}\n'.format(i + 1, p[i]))

def cv(X, y, model, n_iter=1):
    '''
    Z uporabo precnega preverjanja izracuna AUC na ucnih primerih
    '''
    scores = []
    for tr, te in ShuffleSplit(y.size, n_iter, 0.2, random_state=43):
        p = model.fit_predict(X[tr], y[tr], X[te])
        scores.append(auc_score(y[te], p))
    return np.mean(scores)

'''def cv3(X,X1,y, y1, model1,model2, n_iter=1):
    scores = []
    for tr, te in ShuffleSplit(y.size, n_iter, 0.2, random_state=43):
        p = (model1.fit_predict(X[tr], y[tr], X[te]) + model2.fit_predict(X1[tr],y1[tr],X1[te]))/2
        scores.append(auc_score(y[te], p))
    return np.mean(scores)

def cv2(X, y, model, n_iter=1):
    scores = []
    for tr, te in ShuffleSplit(y.size, n_iter, 0.2, random_state=43):
        p = model.fit_predict2(X[tr], y[tr], X[te])
        scores.append(auc_score(y[te], p))
    return np.mean(scores)'''

def feature_pairs(X):
    '''
    V matriko X vstavi vse kombinacije dvojk atributov
    '''
    cs = []
    m = np.max(X, axis=0) + 1
    for i in range(X.shape[1]):
        for j in range(i):
            cs.append(X[:,j] * m[i] + X[:,i])
    return np.column_stack(cs)

def feature_triplets(X):
    '''
    V matriko X vstavi vse kombinacije trojk atributov
    '''
    cs = []
    m = np.max(X, axis=0) + 1
    for i in range(X.shape[1]):
        for j in range(i):
            for k in range(j):
                cs.append(X[:,k] * m[i] * m[j] + X[:,j] * m[i] + X[:,i])
    return np.column_stack(cs)

def compact(X):
    '''
    Mozen razpon vrednosti atributov zmanjsa na najmanjsega moznega
    '''
    for j in range(X.shape[1]):
        X[:,j] = np.unique(X[:,j], return_inverse=True)[1]

def calculate_odds(X,y):
    '''
    Sprejme X (enodimenzionalen array enega atributa) in y in vrne urejeno mnozico
    vrednosti atributa, od najvecje moznosti za 0 do najmanjse
    '''
    dict0=dict()
    dictVsi=dict()
    #Prestej vse ponovitve vsake moznosti ter stevilo nicel, nato deli
    #Kljuc: vrednost atributa, value:st nicel ali st vseh
    for x1,c in itertools.izip(X,range(len(y))):
         if y[c]==0:
             if not (x1 in dict0):
                 dict0[x1]=0.
                 
             dict0[x1]+=1.
         if not (x1 in dictVsi):
             dictVsi[x1]=0.
             if not (x1 in dict0):
                 dict0[x1]=0.
         dictVsi[x1]+=1.
    for x2 in dict0:
        dict0[x2]/=dictVsi[x2]
    sorted_x = sorted(dict0.iteritems(), key=operator.itemgetter(1),reverse=True)
    dictF=dict()
    for x1, c in itertools.izip(sorted_x,range(len(sorted_x))):
        dictF[x1[0]]=c
    return dictF
   

def pcaencode(x):
    '''
    Uporabi PCA da zakodira en stolpec
    Stolpec najprej zakodira z OneHotEncodom, nato pa dobljeno
    matriko reducira
    '''
    xO=OneHotEncoder().fit_transform(X)
    pcaModel=RandomizedPCA(12)
    XT=pcaModel.fit_transform(xO)
    print "DoneOne"
    return XT

def encodeall(X):
    '''
    Z uporabo PCA zakodira vse stolpce matrike X
    Dobimo gosto matriko, ki je priblizno enako dobra kot
    OneHotEncode, vendar veliko manjsa
    '''
    X1=X
    for c in range(X.shape[1]):
        X1=np.hstack((X1,pcaencode(X[:,c])))
    print X1
    return X1


def map_new_values(X,y):
    ''' za nek stolpec X preimenuje vrednosti atributov'''
   # print X
    newDict=calculate_odds(X,y)
    #premikaj se po stolpcu, preimenuj
    for x1,c in itertools.izip(X,range(X.shape[0])):
        #V testni datoteki tudi vrednosti argumontov, ki jih v ucni ni
        if x1 in newDict:    
            X[c]=newDict[x1]
    return X

def remap_X_atributes(X,y):
    '''preimenuje vse stolpce v matriki X, tudi tiste it test'''
    for c in range(X.shape[1]):
        #gremo skozi vsak stolpec, in ga preimenujemo
        X[:,c]=map_new_values(X[:,c],y)
    #print X
    return X

### models ###
class svm:
    def __init__(self):
        self.vm=GaussianNB()
    def fit_predict(self, X, y, X_te):
        self.vm.fit(X,y)
        return self.vm.predict(X_te)

class LogisticRegression:
    '''
    Implementacija logisticne regresije z reguralizacijo
    AUC za l=1.5 okoli 0.882
    '''
    def __init__(self, lambda_):
        self.lambda_ = lambda_

    def cost_grad(self, params, X, y):
        h = 1. / (1. + np.exp(-X.dot(params)))
        cost = -np.mean(np.log(np.where(y, h, 1 - h))) + \
            self.lambda_ * params.dot(params) / (2 * X.shape[0])
        grad = (X.T.dot(h - y) + self.lambda_ * params) / X.shape[0]
        return cost, grad

    def fit_predict(self, X, y, X_te):
        params = np.zeros(X.shape[1])
        params, cost, d = fmin_l_bfgs_b(self.cost_grad, params, args=(X, y))
        return 1. / (1. + np.exp(-X_te.dot(params)))


class ForwardSearch:
    '''
    Z forward searchom izbere n_features najbolj pomombnih argumentov v vhodnih podatkih
    AUC 0.87
    '''
    def __init__(self, model, n_cv_iter, n_features):
        self.model = model
        self.n_cv_iter = n_cv_iter
        self.n_features = n_features

    def fit_predict(self, X, y, X_te):
        mask = np.zeros(X.shape[1], dtype=bool)
        for i in range(self.n_features):
            scores = []
            for j in range(X.shape[1]):
                if not mask[j]:
                    mask[j] = True
                    X_sp = append_ones(OneHotEncoder().fit_transform(X[:,mask]))
                    scores.append((cv(X_sp, y, self.model, self.n_cv_iter), j))
                    mask[j] = False
            score, j = max(scores)
            mask[j] = True

        X_all = np.vstack((X, X_te))
        X_sp = append_ones(OneHotEncoder().fit_transform(X_all[:,mask]))
        return self.model.fit_predict(X_sp[:y.size], y, X_sp[y.size:])


class sktree:
    '''
    Implementacija scikit-learnovega drevesa
    Z pomocjo algoritma PCA na vhodnih podatkih AUC okoli 0.77
    '''
    def __init__(self):
        self.tre= tree.DecisionTreeClassifier(min_samples_leaf=10,min_samples_split=7,criterion="gini")
    def fit_predict(self, X, y, X_te):
        self.tre.fit(X,y)
        return self.tre.predict_proba(X_te)[:,1]

    def fit_predict2(self, X, y, X_te):
        self.tre.fit(X.todense(),y)
        return self.tre.predict_proba(X_te)[:,1]
### data ###

### Random Forest ###
# main.py X rf 100 -> 0.851387108859
class Sklearn:
    '''
    Genericna implementacija, ki podpira vse modele
    iz scikit-learna, ki vkljucuje metodo predict_proba
    V nasem primeru uporabljena za random forest, ki da
    AUC okoli 0.085
    '''
    def __init__(self, model):
        self.model = model

    def fit_predict(self, X, y, X_te):
        self.model.fit(X, y)
        return self.model.predict_proba(X_te)[:,1]

if sys.argv[1] == 'rf':
    from sklearn.ensemble import RandomForestClassifier

    n_estimators = int(sys.argv[2])

    model = Sklearn(RandomForestClassifier(
        n_estimators=n_estimators))
    print cv(X, y, model)



class NaiveBayes:
    '''
    '''
    def __init__(self, alpha):
        self.alpha = alpha

    def fit_predict(self, X, y, X_te):
        m = np.max(np.vstack((X, X_te)), axis=0) + 1

        neg_ind = y == 0
        pos_ind = y == 1

        # fit
        pos_freq = []
        neg_freq = []
        for j in range(X.shape[1]):
            pos = np.bincount(X[pos_ind,j], minlength=m[j])
            neg = np.bincount(X[neg_ind,j], minlength=m[j])

            pos_freq.append((pos + self.alpha) / (float(np.sum(pos)) +
                self.alpha * m[j]))
            neg_freq.append((neg + self.alpha) / (float(np.sum(pos)) +
                self.alpha * m[j]))
        pos_prior = float(np.sum(pos_ind)) / X.shape[0]

        # predict
        p = []
        for i in range(X_te.shape[0]):
            pos = np.log(pos_prior)
            neg = np.log(1 - pos_prior)

            for j in range(X_te.shape[1]):
                pos += np.log(pos_freq[j][X_te[i,j]])
                neg += np.log(neg_freq[j][X_te[i,j]])
            p.append(pos - neg)

        return p


def cv_stack(X, y, model):
    '''
    Zapise podatke iz danega modela v obliki, primerni za stacking
    (stolpec iz 5 predictionov na testnem modelu + prediction na ostalih podatkih)
    '''
    print 'cv_stack'
    p = np.zeros(X.shape[0])
    for tr, te in KFold(y.size, 5):
        p[te] = model.fit_predict(X[tr], y[tr], X[te])
    p[y.size:] = model.fit_predict(X[:y.size], y, X[y.size:])
    pickle.dump(p, open(submit_fname, 'wb'), 2)

##Imena datotek za branje in pisanje##
dataset = sys.argv.pop(1)
submit_fname = 'res/{}.pkl'.format('_'.join(sys.argv[1:]))


##BRANJE PODATKOV###
if sys.argv[1] == 'data':
    D_tr = np.loadtxt('data/train.csv', skiprows=1, delimiter=',', dtype=int)
    D_te = np.loadtxt('data/test.csv', skiprows=1, delimiter=',', dtype=int)

    X = np.vstack((D_tr[:,1:9], D_te[:,1:9]))
    y = D_tr[:,0]

### <NOVO>
    compact(X)
    D = []
    if 'X' in dataset:
        D.append(X)
    if 'P' in dataset:
        D.append(feature_pairs(X))
    if 'T' in dataset:
        D.append(feature_triplets(X))
    X = np.hstack(D)
    compact(X)

    X_sp = OneHotEncoder().fit_transform(X)

    pickle.dump(X_sp, open('pkl/X_sp_{}.pkl'.format(dataset), 'wb'), -1)
    pickle.dump(X, open('pkl/X_{}.pkl'.format(dataset), 'wb'), -1)
    pickle.dump(y, open('pkl/y.pkl', 'wb'), -1)

np.random.seed(43)

X = pickle.load(open('pkl/X_{}.pkl'.format(dataset), 'rb'))
y = pickle.load(open('pkl/y.pkl', 'rb'))

###MAIN###
### Logistic Regression ###
if sys.argv[1] == 'lr':
    X_sp = pickle.load(open('pkl/X_sp_{}.pkl'.format(dataset), 'rb'))
    lambda_ = float(sys.argv[2])
    model = LogisticRegression(lambda_)

    X_sp = append_ones(X_sp)
    X= append_ones(X)
    #print cv(X,y,model)
    print cv(X_sp, y, model)


### FORWARD SEARCH ###
if sys.argv[1] == 'fs':
    lambda_ = float(sys.argv[2])
    n_cv_iter = int(sys.argv[3])
    n_features = int(sys.argv[4])

    model = ForwardSearch(LogisticRegression(lambda_), n_cv_iter, n_features)
    print cv(X, y, model)


### DECISION TREE ###
if sys.argv[1]== 'skt':
    model=sktree()
   #X_sp = append_ones(X_sp)
    print cv(X, y, model)


    ###ZAKODIRA MATRIKO X Z PCA ###
if sys.argv[1]=='test':
    test=encodeall(X)
    pickle.dump(test, open('pkl/X_test12.pkl'.format(dataset), 'wb'), -1)


    ###DECISION TREE Z PCA ###
if sys.argv[1]=='test2':
    Xtest2=pickle.load(open('pkl/X_test.pkl'.format(dataset), 'rb'))
    model=sktree()
    print cv_stack(Xtest2, y, model)


    ### RANDOM FOREST Z PCA ###
if sys.argv[1]=='test3':
    Xtest= pickle.load(open('pkl/X_test12.pkl'.format(dataset), 'rb'))
    n_estimators = int(sys.argv[2])
    model = Sklearn(RandomForestClassifier(
        n_estimators=n_estimators))
    cv_stack(Xtest, y, model)

###STACKING###
if sys.argv[1]=='stack':
    
    #Modeli, ki jih zdruzujemo, v direktoriju /res
    model=[
           'lr_0.5',
           'lr_1',
           'lr_1.5',
           'lr_2',
           'test2_1',
           'test3_100',
           'test2',
           ]
    ps=[]
    for f in model:
        ps.append(pickle.load(open('res/{}.pkl'.format(f),'rb')))
    lambda_=float(sys.argv[2])
    P = np.column_stack(ps)
    y = pickle.load(open('pkl/y.pkl', 'rb'))
    
    #izbira modela, s katerim predvidevamo u?inkovitost združenih modelov
    model=LogisticRegression(lambda_)
    print cv(append_ones(P), y, model,n_iter=3)

