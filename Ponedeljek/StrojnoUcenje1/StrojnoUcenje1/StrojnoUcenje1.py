'''
h=t0+t1*x
Odvodi:
t0=1/m* SUM(h(xi)-yi)
t1=1/m * SUM(h(xi)-yi)*xi)

Algoritem:
t0,t1=0,0
Ponavljaj:
    t0,t1=odvod(t0),odvod(t1)


Numpy:
matrika.shape[0] = st vrstic
matrika.shape[1] = st stolpcev
'''
import itertools
'''import pylab as plt'''
import numpy as np

def dataset1():
    m = 50
    np.random.seed(42)
    x = np.random.randn(m)
    y = 1 + 2 * x + 0.5 * np.random.randn(m)
    return x, y

def dataset2():
    m, n = 50, 3
    np.random.seed(42)
    X = np.random.randn(m, n)
    params = [1, -1, 2]
    y = 1 + X.dot(params) + 0.1 * np.random.randn(m)
    return X, y

def h(t0,t1,x):
    return t0+t1*x

def sum0(t0,t1,x,y):
    '''
    Pomozna funkcija za odvod t0
    '''
    s=0.
    for cx,cy in itertools.izip(x,y):
        s+=t0+(t1*cx)-cy
    return s

def sum1(t0,t1,x,y):
    '''
    Pomozna funkcija za odvod t1
    '''
    s=0.
    for cx,cy in itertools.izip(x,y):
        s+=(t0+(t1*cx)-cy)*cx
    return s

def j(t0, t1, x, y):
    '''
    Izracuna vrednost kriterijske funkcije
    '''
    count = 0.
    sum=0.
    for cx,cy in itertools.izip(x,y):
        sum+=((t0+t1*cx) - cy) * ((t0+t1*cx) - cy)
        count+=1
    return sum/(2*count)

def gradient_descent(alpha, theta0, theta1, x, y,l=10):
    '''
    Izracuna vrednosti theta0 in theta1 s pomocjo gradientnega spusta
    Zadnji argument poda stevilo ponovitev (default=10)
    '''
    t0=0.
    t1=0.
    for c in range(l):
        print j(t0,t1,x,y), t0, t1
        t0,t1=t0-alpha*(1/float(len(x)))*sum0(t0,t1,x,y),t1-alpha*(1/float(len(x)))*sum1(t0,t1,x,y)
    return t0,t1

'''
def visualize(theta0, theta1, x, y):
    t0s = np.linspace(-5, 8, 100)
    t1s = np.linspace(-5, 5, 100)
    cost = [[j(t0, t1, x, y) for t0 in t0s] for t1 in t1s]

    plt.subplot(211)
    plt.contour(t0s, t1s, np.log(cost))
    plt.plot(theta0, theta1, 'ro')
    plt.xlabel('$\Theta_0$')
    plt.ylabel('$\Theta_1$')

    plt.subplot(212)
    plt.plot([-3, 3], [theta0 - 3 * theta1, theta0 + 3 * theta1])
    plt.plot(x, y, 'o')
    plt.xlabel('$x$')
    plt.ylabel('$y$')
    plt.show()
   '''

def append_ones(matrika):
     return np.column_stack((np.ones(X.shape[0]), X))
     '''return np.concatenate((np.ones((matrika.shape[0],1)),matrika), axis=1)'''

def cost_grad(theta, X, y):
    print theta
    print X
    t = X.dot(theta) - y
    kfunkc = np.sum((np.square(X.dot(theta)-y)))/(2*X.shape[0])
    grad = X.T.dot(t) / X.shape[0]
    return kfunkc,grad

def gradient_descent2(alpha, theta, X, y):
    for i in range(10):
        cost, grad = cost_grad(theta, X, y)
        print cost, theta
        theta -= alpha * grad
    return theta
    
x, y = dataset1()
t0, t1 = gradient_descent(1, 0, 0, x, y)
print "Vrednost kriterijske funkcije:"
print j(1, 0.5, x, y)
print "Vrednosti theta0 in theta1:"
print t0, t1
X, y = dataset2()
X = append_ones(X)
print cost_grad(np.ones(X.shape[1]), X, y)