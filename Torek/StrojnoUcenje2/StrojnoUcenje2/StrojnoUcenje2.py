'''
h=t0+t1*x
Odvodi:
t0=1/m* SUM(h(xi)-yi)
t1=1/m * SUM(h(xi)-yi)*xi)

Algoritem:
t0,t1=0,0
Ponavljaj:
    t0,t1=odvod(t0),odvod(t1)


Numpy:
matrika.shape[0] = st vrstic
matrika.shape[1] = st stolpcev

np.eye(5) - identiteta 5x5
np.random.random((m,n))-nakljucna matrika m*n
matrika[m,n]-element m-te vrstice in n-tega stolpca
matrika[x]- x-ta vrstica
matrika[:,x]- x-ti stolpec
matrika[[0,1,2,0]] - matrika z 0, 1, 2, 0 vrstico prvotne
np.exp(matrika) - vsak element je e^element xa
X.mean() - povprecje
X.mean(axis=0) - povprecje vsakega stolpca
X.mean(axis=1) - povprecje vsake vrstice
matrika.std() - standardna deviacija
matrika.max() - najvecji element
matrika.min() - najmanjsi element
'''
import pickle
import itertools
import numpy as np
# import matplotlib.pyplot as plt
import math
from scipy.optimize import fmin_l_bfgs_b
from sklearn.preprocessing import OneHotEncoder

def dataset1():
    m, n = 5, 3
    np.random.seed(42)
    X = np.random.randn(m, n)
    params = [1, -1, 2]
    y = 1 + X.dot(params) + 0.1 * np.random.randn(m)
    return X, y

def dataset1L():
    m = 5
    np.random.seed(42)
    X = np.random.randn(m, 3)
    params = [1, -1, 2]
    z = 1 + X.dot(params) + .1 * np.random.randn(m)
    return X, (z > 0).astype(int)

def dataset2():
    '''Prvi atribut pomnozimo s 1000 in mu pristejemo 100'''
    m = 50
    np.random.seed(42)
    X = np.random.randn(m, 3)
    X[:,0] = X[:,0] * 1000 + 100
    params = [1, -1, 2]
    y = 1 + X.dot(params) + 0.1 * np.random.randn(m)
    return X, y

def dataset3():
    '''Dataset for polynomial regression'''
    m = 50
    np.random.seed(42)
    x = np.random.randn(m)
    X = np.column_stack((x, x*x))
    params = [-1, 1]
    y = 1 + X.dot(params) + 0.2 * np.random.randn(m)
    return x[:,None], y

def dataset4():
    '''Podatki z eno kategoricno spremenljivko'''
    m = 50
    np.random.seed(42)
    x = np.random.randint(3, size=m)
    X = np.eye(3)[x]
    params = [1, 2, -1]
    y = 1 + X.dot(params) + 0.1 * np.random.randn(m)
    return x[:,None], y


def h(t0,t1,x):
    return t0+t1*x

def sum0(t0,t1,x,y):
    '''
    Pomozna funkcija za odvod t0
    '''
    s=0.
    for cx,cy in itertools.izip(x,y):
        s+=t0+(t1*cx)-cy
    return s

def sum1(t0,t1,x,y):
    '''
    Pomozna funkcija za odvod t1
    '''
    s=0.
    for cx,cy in itertools.izip(x,y):
        s+=(t0+(t1*cx)-cy)*cx
    return s

def j(t0, t1, x, y):
    '''
    Izracuna vrednost kriterijske funkcije
    '''
    count = 0.
    sum=0.
    for cx,cy in itertools.izip(x,y):
        sum+=((t0+t1*cx) - cy) * ((t0+t1*cx) - cy)
        count+=1
    return sum/(2*count)

def gradient_descentOLD(alpha, theta0, theta1, x, y,l=10):
    '''
    Izracuna vrednosti theta0 in theta1 s pomocjo gradientnega spusta
    Zadnji argument poda stevilo ponovitev (default=10)
    '''
    t0=0.
    t1=0.
    for c in range(l):
        print j(t0,t1,x,y), t0, t1
        t0,t1=t0-alpha*(1/float(len(x)))*sum0(t0,t1,x,y),t1-alpha*(1/float(len(x)))*sum1(t0,t1,x,y)
    return t0,t1


def append_ones(matrika):
     return np.column_stack((np.ones(X.shape[0]), X))
     '''return np.concatenate((np.ones((matrika.shape[0],1)),matrika), axis=1)'''

def cost_grad(theta, X, y,lambda_=0):
    t = X.dot(theta) - y
    kfunkc = (np.sum((np.square(X.dot(theta)-y))) + lambda_*np.sum(np.array(theta)**2))/(2.*X.shape[0])
    grad = ( (X.T.dot(t)) + (lambda_*np.array(theta)) )/ (X.shape[0])
    return kfunkc,grad

def gradient_descent(alpha, theta, X, y):
    for i in range(10):
        cost, grad = cost_grad(theta, X, y)
        #print cost, theta
        theta -= alpha * grad
    return theta

def standardize(X):
    return (X-X.mean(axis=0))/X.std(axis=0)
    
def one_hot(x):
    return np.eye(np.max(x)+1)[x]

def h_log(theta,X):
    h=X.dot(theta)
    ret = []
    for x1 in h:
        ret.append(logist(x1))
    return ret

#def logist(x):
  #  return 1/(1+math.exp(-x))

#def j_log(theta,x,y):
    #return y*math.log(h_log(x))+(1-y)*math.log(1-h_log(x))

def cost_grad_log(theta, X, y,lambda_=0):
    '''
    Izracuna vrednost kriterijske funkcije in gradient za logisticno regresijo
    Argumenti: Zacetni parametri, argumenti X, vrendosti y
    '''
    sum1=0

    # to so vrednosti t0+t1*x1+t2*x2+...
    h=X.dot(theta)

    #to operacijo opravimo na vsakem elementu matrike h
    h=1./(1.+np.exp(-h))

    #sedaj izra?unamo xj*(h0(xi)-yi) (xj so elementi vrstic iz Xt, h(x) so elementi stolpcev iz h)
    hy=(X.T.dot(h-y) + lambda_*np.array(theta))/len(y)

    #ekvivalentno spodnji zanki
    vsota=-sum((y*np.log(h) + (1-y)*np.log(1-h)))/len(y) + (lambda_*np.sum(np.array(theta)**2))/(2.*len(y))
    # for x1, y1 in itertools.izip(h,y):
       # sum1+=y1*math.log(x1)+(1-y1)*math.log(1-x1)
    return vsota,hy
        



X=np.array([[1,5],[2,7],[3,9]])
print X
X-=X.min(axis=0)
print X
X2= np.array([[1, 1], [0, 2], [-1, 3]])
print "Standardizirani stolpci:"
print standardize(X2)

#Pred standardizacijo
X, y = dataset2()
print "Pred standardizacijo:"
print gradient_descent(1.0, [0, 0, 0, 0], append_ones(X), y)

# po standardizaciji
X = standardize(X)
print "Po standardizaciji:"
print gradient_descent(1.0, [0, 0, 0, 0], append_ones(X), y)
print one_hot([0, 1, 2, 1, 1, 0])

print "brez one_hot kodiranja"
X, y = dataset4()
print gradient_descent(1.0, [0, 0], append_ones(X), y)

print "z one_hot kodiranjem"
X = one_hot(X[:,0])
print gradient_descent(1.0, [0, 0, 0, 0], append_ones(X), y)

X, y= dataset3()
#Pretvorimo podatke v obliko 1, x, x^2
X2=np.column_stack((np.ones(X.shape[0]), X, X*X))
print "Polinomna regresija"
theta=gradient_descent(0.5, [0,0,0], X2, y)
print theta
t2=theta[::-1]#ker polyval ho?e faktorje v takem vrstnem redu
#plt.plot(X,y,'ro')
#x = np.linspace(-2, 2, 100)
#plt.plot(x,np.polyval(t2,x),'k-')
#plt.show()


#uporaba l-bfgs
X, y = dataset1()
print cost_grad_log([1, 1, 1], X, y)
X = append_ones(X)
params, cost, d = fmin_l_bfgs_b(cost_grad_log, [0,0,0,0], args=(X, y))
print params

#Linearna regresija, primerjava z regularizacijo
X, y = dataset1()
print cost_grad(np.ones(3), X, y, 0)
print cost_grad(np.ones(3), X, y, 1)

#Logisticna regresija, primerjava z regularizacijo
X, y = dataset1L()
print cost_grad_log(np.ones(3), X, y, 0)
print cost_grad_log(np.ones(3), X, y, 1)

#Branje podatkov
#file_train = np.loadtxt('C:\Users\urban.skvorc\Documents\StrojnoUcenje\Data\\train.csv',skiprows=1,delimiter=',',dtype=int)
#file_test = np.loadtxt('C:\Users\urban.skvorc\Documents\StrojnoUcenje\Data\\test.csv',skiprows=1,delimiter=',',dtype=int)
#X= np.vstack((file_train[:,1:9], file_test[:,1:9]))
#y=file_train[:,0]
#print X,y
#X_sp= OneHotEncoder().fit_transform(X)

X_sp=pickle.load(open( 'C:\\Users\\urban.skvorc\\Documents\\StrojnoUcenje\\Data\\x_sp.pki','rb'))
X=pickle.load( open('C:\\Users\\urban.skvorc\\Documents\\StrojnoUcenje\\Data\\x.pki','rb'))
y=pickle.load(open( 'C:\\Users\\urban.skvorc\\Documents\\StrojnoUcenje\\Data\\y.pki','rb'))

#pickle.dump(X_sp, open('C:\Users\urban.skvorc\Documents\StrojnoUcenje\Data\\x_sp.pki','wb'),-1)
#pickle.dump(X, open('C:\Users\urban.skvorc\Documents\StrojnoUcenje\Data\\x.pki','wb'),-1)
#pickle.dump(y, open('C:\Users\urban.skvorc\Documents\StrojnoUcenje\Data\\y.pki','wb'),-1)